Object.prototype[Symbol.toPrimitive] = function (hint) {
    let item;

    if (hint === "number") {
        console.log("Number")
        item = this.valueOf();
        if (typeof (item) !== "number") {
            item = this.toString();
            if (typeof (item) !== "string") {
                throw new TypeError('Cannot convert object to primitive value ')
            }
        }
    }
    else if (hint === "string") {
        console.log("String")
        item = this.toString();
        if (typeof (item) !== "string") {
            item = this.valueOf();
            if (typeof (item) !== "number") {
                throw new TypeError('Cannot convert object to primitive value ')
            }
        }
    } else {
        console.log("default")
        item = this.toString();
    }

    return item;
}



let obj = {};

// string 
alert(obj);
String(obj);
// string 

//number
Number(obj);
obj / "4";
+obj;
//number

//default
obj + 4;
"3" + obj;
obj == "default";
//default
